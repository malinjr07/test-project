import { initializeApp } from 'firebase/app';
import { getFirestore } from 'firebase/firestore';

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: 'AIzaSyCy2OQGXaDWSAVn7KC3FLHfc_4Z4Bn0Ywc',
  authDomain: 'test-01-e9d79.firebaseapp.com',
  projectId: 'test-01-e9d79',
  storageBucket: 'test-01-e9d79.appspot.com',
  messagingSenderId: '985279242903',
  appId: '1:985279242903:web:9029e2ff5665283d637fd2',
  measurementId: 'G-YBSFX14N93',
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

// Initialize Cloud Firestore and get a reference to the service
const db = getFirestore(app);

export default db;
