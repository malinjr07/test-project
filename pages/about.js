import React, { useState } from 'react';

import db from '../config/firebase';
import { collection, addDoc } from 'firebase/firestore';

const About = () => {
  const [name, setName] = useState('');

  const addData = async (e) => {
    e.preventDefault();
    // console.log('name is:', name);
    // await addDoc(db, 'users', {
    //     name: name,
    //     age: 30,
    // });

    const submit = await addDoc(collection(db, 'users'), {
      name: name,
    });

    console.log('submit is:', submit);
  };

  return (
    <form
      onSubmit={addData}
      method='post'
      className='h-screen flex justify-center items-center w-screen flex-col '
    >
      <div className=''>
        <label
          htmlFor='name'
          className=' text-left p-3 font-medium text-lg text-gray-800'
        >
          Name
        </label>
        <input
          type='text'
          id='name'
          name='name'
          onChange={(e) => setName(e.target.value)}
          className='rounded-sm border border-gray-600 px-3 py-2 my-2 '
        />
      </div>
      <button
        type='submit'
        className='py-1 px-3 rounded-md border-gray-700 bg-blue-500 text-white mt-5'
      >
        Submit
      </button>
    </form>
  );
};

export default About;
